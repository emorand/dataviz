---
title: "DataViz avec R - les cartes"
subtitle: "![](images/LOGO_ined1c.png)" 
author:
- "Bénédicte Garnier et Elisabeth Morand"
- "Service Méthodes Statistiques"
date: "18 janvier 2022"
output: 
  xaringan::moon_reader:
    css: xaringan-themer.css
    nature:
      slideNumberFormat: "%current%"
      highlightStyle: github
      highlightLines: true
      ratio: 16:9
      countIncrementalSlides: true
      header-includes:
      - \usepackage{titling}
      - \pretitle{\begin{center} \includegraphics[width=2in,height=2in]{![](images/LOGO_ined1.png)}\LARGE\\}
      - \posttitle{\end{center}}
      
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(
  fig.width=9, fig.height=3.5, fig.retina=3,
  out.width = "100%",
  cache = FALSE,
  echo = TRUE,
  message = FALSE, 
  warning = FALSE,
  fig.show = TRUE,
  hiline = TRUE
)
```

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
library(xaringan)
style_duo_accent(
  primary_color = "#c52533",
  secondary_color = "#FF961C",
  inverse_header_color = "#FFFFFF"
)
```


## Sommaire

- La conception d'une carte
- Identifier la finalité de la carte
- Les concepts de la cartographie
- Les variables visuelles
- Les règles de la conception cartographique
- Choix des couleurs
- Travailler l’habillage
- Exporter la carte produite
- Faire des cartes avec la package mapsf

---

## La conception d'une carte

Une carte est *une représentation possible* des données
Elle constitue un point de vue 
On peut avoir des données fausses déguisées en jolie carte … et des jolies données mal cartographiées 
Il faut respecter des *règles*
Une carte est un graphique qui utilise la *communication* par les *signes* avec un *langage visuel* spécifique et le *système spatial*

C’est un outil de *data visualization*

---

## Identifier la finalité de la carte

Son *but*

- explorer, analyser, interpréter, comparer (carte thématique)
- montrer et/ou situer (carte topographique)
- convaincre (story telling) – usage du pathos (cf neocarto)
- son public/lecteur 
- son support : papier, écran, web (desktop/mobile)

---

## Les concepts de la cartographie

Acquérir un ensemble de données cohérent, structuré et exploitable 

- Le fond de carte (informations géométriques)
- Les données attributaires (informations statistiques)
- Construire la carte
- Utiliser des variables visuelles et respecter la sémiologie graphique
- Produire des cartes *classiques*: en stock, qualitatives, choroplèthes 
ou d'autres types de cartes : Cartes de flux, lissées, de discontinuités, anamorphoses
- Mettre en scène, habiller la carte 
- Exporter la carte

Utiliser des ressources existantes: les shapefiles, GeoJson
Des outils gratuits  : Magrit / QGIS (SIG)

Savoir créer ou modifier un fond de carte 


---

##  La cartographie automatique

<center><img src="images/Carto_automat.PNG" height="350px" /></center>

---

## Les règles de la conception cartographique

Pour transmettre une information correcte ... la nature et le type de la mesure détermine le type de carte

![](images/Regles.PNG)

---

## Variables visuelles et implantation des données
 (d’après Bertin)

![](images/var visuelles.PNG)

---

## Choix des couleurs

https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3

![](images/Brewer.PNG)

---

## La mise en scène : travailler l’habillage

Déposer les **éléments de la légende** : titre, légende, échelle, source, date
Intégration de symboles, logos, textes travaillés, 
étiquettes/labels et modification des couleurs

Et aussi (pas toujours indispensable ) : orientation, coordonnées, nomenclature, cartouche, cadre

Si besoin Grossissement de certains détails avec un outil de dessin (et si la carte est exportée au format vectoriel)

--- 

## Exporter la carte produite

Format *vectoriel* (.svg) ou (.pdf) peut être  utilisé avec un éditeur vecteur comme Illustrator ou Inkscape

Format *raster* (.png) = image composée de pixels. Parfois suffisant (en utilisant les fonctionnalités de dessin des logiciels Paint ou Word ou PowerPoint, …)

Format *geo* : utilisable avec un logiciel traitant des données géométriques (Json, Shapefiles, …) (SIG comme QGIS)

---

## Application avec le package mapsf
https://riatelab.github.io/mapsf/articles/mapsf.html#symbology

---

## Références

* Manuel de cartographie de référence :
Lambert, N., Zanin, C. (2016). Manuel de cartographie: Principes, méthodes, applications. France: Armand Colin.

* Exemples de carte :
https://magrit.hypotheses.org/galerie
https://neocarto.hypotheses.org/
https://visionscarto.net

* Discrétiser pour cartographier
https://magrit.hypotheses.org/288#more-288

* Le package R mapsf 
https://riatelab.github.io/mapsf/articles/mapsf.html#symbology

* Cheat sheets :  
https://www.rstudio.com/resources/cheatsheets/
  
* Séminaire RUSS :  
https://russ.site.ined.fr/  

---

