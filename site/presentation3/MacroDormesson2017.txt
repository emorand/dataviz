�Messieurs les pr�sidents, Monsieur le Premier ministre, Mesdames et Messieurs les ministres, Mesdames et Messieurs les parlementaires, Mesdames et Messieurs les acad�miciens, Mesdames et Messieurs les membres du corps pr�fectoral, Mesdames et Messieurs les membres du corps diplomatique, ch�re Fran�oise d'Ormesspn, ch�re H�lo�se d'Ormesson, chers membres de la famille, ch�re Marie-Sarah, Mesdames et Messieurs.

�Si claire est l'eau de ces bassins, qu'il faut se pencher longtemps au dessus pour en comprendre la profondeur�. Ces mots sont ceux qu'Andr� Gide �crit dans son Journal � propos de la Bruy�re.

Ils conviennent particuli�rement � Jean d'Ormesson.

Car plus qu'aucun autre il aima la clart�. Celle des eaux de la M�diterran�e, dont il raffolait, celle du ciel d'Italie, celle des maisons blanches de Simi, cette �le secr�te des �crivains. Celle des pentes enneig�es et �clatantes o� il aimait � skier, comme celles des criques de la c�te turque, inond�es de soleil.

Ne fut-il pas lui-m�me un �tre de clart�?

Il n'�tait pas un lieu, pas une discussion, pas une circonstance, que sa pr�sence n'illumin�t. Il semblait fait pour donner aux m�lancoliques le go�t de vivre et aux pessimistes celui de l'avenir.

Il �tait trop conscient des ruses de l'Histoire pour se navrer des temps pr�sents, et sa conversation, elle-m�me, �tait si �tincelante qu'elle nous consolait de tout ce que la vie, parfois, peut avoir d'amer.

    �Cette gr�ce lumineuse, contagieuse, a conquis ses lecteurs qui voyaient en lui un antidote � la grisaille des jours�

Jean d'Ormesson fut ainsi cet homme entour� d'amis, de camarades, offrant son amiti� et son admiration avec enthousiasme, sans mesquinerie. Ce fut un �go�ste passionn� par les autres. Sans doute son br�viaire secret, �tait-il Les Copains de Jules Romains, auquel il avait succ�d� � l'Acad�mie fran�aise. Berl, Caillois, Hersch, Mohrt, D�on, Marceau, Rheims, Sureau, Rouart, Deniau, Fumaroli, Nourissier, Orsenna, Lambron ou Baer� je ne peux les citer tous, mais cette cohorte d'amis, ce furent des vacances, des po�mes r�cit�s, de la libert� partag�e.

Pour ceux qu'il accompagna jusqu'au terme ultime, sa pr�sence et sa parole furent des baumes incomparables. Comme son cher Chateaubriand le disait de Ranc�, �on croyait ne pouvoir bien mourir qu'entre ses mains, comme d'autres y avaient voulu vivre�.

Cette gr�ce lumineuse, contagieuse, a conquis ses lecteurs qui voyaient en lui un antidote � la grisaille des jours. Paul Morand disait de lui qu'il �tait un �gracieux d�vorant�, rendant la vie int�ressante � qui le croisait. C'est cette clart� qui d'abord nous manquera, et qui d�j� nous manque en ce jour froid de d�cembre.

Jean d'Ormesson fut ce long �t�, auquel, pendant des d�cennies, nous sommes chauff�s avec gourmandise et gratitude. Cet �t� fut trop court, et d�j� quelque chose en nous est assombri.

Mais celui que l'on voyait caracoler, dou� comme il l'�tait pour l'existence et le plaisir, n'�tait pas le ludion auquel quelques esprits chagrins tent�rent, d'ailleurs en vain, de le r�duire.

    �Jean d'Ormesson �tait de ceux qui nous rappelaient que la l�g�ret� n'est pas le contraire de la profondeur, mais de la lourdeur�

La France est ce pays complexe o� la gaiet�, la qu�te du bonheur, l'all�gresse, qui furent un temps les atours de notre g�nie national, furent un jour, on ne sait quand, comme frapp�s d'indignit�. On y vit le signe d'une absence condamnable de s�rieux ou d'une l�g�ret� forc�ment coupable. Jean d'Ormesson �tait de ceux qui nous rappelaient que la l�g�ret� n'est pas le contraire de la profondeur, mais de la lourdeur.

Comme le disait Nietzsche de ces Grecs anciens, parmi lesquels Jean d'Ormesson e�t r�v� de vivre, il �tait �superficiel par profondeur�.

Lorsqu'on a re�u en partage les facilit�s de la lign�e, du talent, du charme, on ne devient normalement pas �crivain, on ne se veut pas � toute force �crivain, sans quelques failles, sans quelques intranquillit�s secr�tes et f�condes.

�J'�cris parce que quelque chose ne va pas� disait-il, et lorsqu'on lui demandait quoi, il r�pondait: �Je ne sais pas�. Ou, plus �vasivement encore: �Je ne m'en souviens plus.� Telle �tait son �l�gance dans l'inqui�tude.

Et c'est l� que l'eau claire du bassin soudain se trouble. C'est l� que l'exquise transparence laisse para�tre des ombres au fond du bleu cobalt. Un jour vint o� Jean-qui-rit admit la pr�sence tenaillante, irr�fragable, d'un manque, d'une f�lure, et c'est alors qu'il devint �crivain.

Ses yeux aujourd'hui se sont ferm�s, le rire s'est tu, et nous voici, cher Jean, face � vous. C'est-�-dire face � vos livres. Tous ceux que vous aviez �gar�s par vos diversions, que vous aviez accabl�s de votre modestie, tous ceux � qui vous aviez assur� que vous ne dureriez pas plus qu'un d�jeuner de soleil, sont face � cette �vidence, dont beaucoup d�j� avaient conscience, se repassant le mot comme un secret.

Cette �vidence, c'est votre �uvre. Je ne dis pas: vos livres, je ne dis pas: vos romans. Je dis: votre �uvre. Car ce que vous avez construit avec la nonchalance de qui semble ne pas y tenir, se tient devant nous, avec la force d'un �difice o� tout est voulu et pens�, o� l'on reconna�t � chaque page ce que les historiens de l'art appellent une palette, c'est-�-dire cette riche vari�t� de couleurs que seule la singularit� d'un regard unit.

La clart� �tait trompeuse, elle �tait un miroir o� l'on se leurre, et le temps est venu pour vous de faire mentir votre cher Toulet. �Que mon linceul au moins me serve de myst�re�, �crivait-il. Votre linceul, lui, d�sormais vous r�v�le.

Nous devrons, pour vous entendre, � pr�sent tendre l'oreille, et derri�re les accords majeurs nous entendrons, comme chez Mozart, la nuance si profonde des accords mineurs.

Ce que votre politesse et votre pudeur tentaient de nous cacher, vous l'aviez mis dans vos livres. Et ce sont les demi-teintes, le �sfumato� subtil, qui vont � pr�sent colorer la surface claire. Ce sont ces mille couleurs qui flottent comme sur de la �moire� pr�cis�ment, dont Cocteau parlait en essayant de qualifier les blancs de C�zanne. Nous ne vous d�couvrirons ni triste, ni sombre, mais derri�re votre ardeur nous saurons voir une fi�vre, derri�re vos plaisirs une insatisfaction, et derri�re votre bonheur quelque chose d'�perdu, de haletant, qui nous touche en plein c�ur.

    �C'est ainsi que vous avez nou� avec les Fran�ais (...) une relation particuli�re, une proximit� en humanit� qui n'�tait qu'� vous.�

Nous entrerons dans le secret de cette �me qui s'est si longtemps pr�tendue incr�dule pour comprendre qu'elle ne cessa d'embrasser le monde avec une ferveur mystique, d�busquant partout, au c�ur de son ordre improbable et �vident, ce Dieu, au fond si mal cach�, dont vous esp�riez et redoutiez la pr�sence et qui, peut-�tre, dans quelque empyr�e, vous fit enfin: �La f�te continue.�

Vous ne nous aviez pas si bien tromp�s, il est vrai. Nous savons que votre conversation la plus personnelle �tait r�serv�e � ces �crivains que fascin�rent les myst�res du monde, et d'abord l'insondable myst�re du temps. Cheminer avec Saint-Augustin, Chateaubriand, Proust, c'est n'�tre point dupe des arcanes de la vie. S'entretenir par-del� la mort avec Caillois, Berl, ou votre p�re, c'est frayer dans des contr�es parfois aust�res o� vous alliez nourrir la force de vos livres. C'est dans ces confrontations intimes que vous alliez puiser cette �nergie incomparable. Contrairement � Chateaubriand, encore lui, qui se d�sesp�rait de durer, vous avez cru qu'en plongeant au c�ur des ab�mes de la vie vous trouveriez la mati�re revigorante et universelle de livres o� chacun reconna�trait sa condition, o� chacun se consolerait de ses contradictions.

Et pour cela vous avez invent�, presque sans la chercher, cette forme nouvelle tenant de l'essai, de l'entretien, de la confession et du r�cit, une conversation tant�t profonde, tant�t l�g�re, un art libertin et m�taphysique. C'est ainsi que vous avez nou� avec les Fran�ais, et avec vos lecteurs dans tant de pays, une relation particuli�re, une proximit� en humanit� qui n'�tait qu'� vous.

Le courage de l'absolu dans la politesse d'un sourire.

C'est cela votre �uvre, elle vous lie � Montaigne, � Diderot, � La Fontaine et Chateaubriand, � Pascal et Proust, elle vous lie � la France, � ce que la France a de plus beau et de plus durable: sa litt�rature.

    �Nous vous demandons pardon, Monsieur, de ne pas vous avoir tout � fait �cout�, pardon pour cette pompe qui n'ajoute rien � votre gloire�

C'est le moment de dire, comme Mireille � l'enterrement de Verlaine: �Regarde, tous tes amis sont l�.� Oui, nous sommes l�, divers par l'�ge, par la condition, par le m�tier, par les opinions politiques, et pourtant profond�ment unis par ce qui est l'essence m�me de la France: l'amour de la litt�rature et l'amiti� pour les �crivains. Et ce grand mouvement qu'a provoqu� votre mort, cette masse d'�motion, derri�re nous, derri�re ces murs, autour de nous et dans le pays tout entier, n'a pas d'autres causes. � travers vous la France rend hommage � ce que Rinaldi appelait �la seule chose s�rieuse en France, si l'on raisonne � l'�chelle des si�cles�.

�voquant, dans un livre d'entretien, votre enterrement, vous aviez �crit: �� l'enterrement de Malraux, on avait mis un chat pr�s du cercueil, � celui de Defferre c'�tait un chapeau, moi je voudrais un crayon, un crayon � papier, les m�mes que dans notre enfance. Ni �p�e, ni L�gion d'honneur, un simple crayon � papier.�

Nous vous demandons pardon, Monsieur, de ne pas vous avoir tout � fait �cout�, pardon pour cette pompe qui n'ajoute rien � votre gloire. Avec un sourire auriez-vous pu dire peut-�tre que nous cherchions l� � vous attraper par la vanit� et peut-�tre m�me que cela pourrait marcher.

Non, cette c�r�monie, Monsieur, nous permet de manifester notre reconnaissance et donc nous rassure un peu. Du moins puis-je, au nom de tous, vous rester fid�le en d�posant sur votre cercueil ce que vous allez et ce que vous aviez voulu y voir, un crayon, un simple crayon, le crayon des enchantements, qu'il soit aujourd'hui celui de notre immense gratitude et celui du souvenir.

Je vous remercie. 